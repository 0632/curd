# curd
The ThinkPHP 8 Curd Package

## 安装
> composer require wym/curd

## 使用
### 快速生成应用
```php
php think wym:app demo
```
执行上面的指令会自动生成demo应用，自动生成的应用目录包含了controller、view目录以及common.php、middleware.php、event.php、Admin.php控制器、Index.php控制器、对应视图模板等文件。如下图：
```html
www  WEB部署目录（或者子目录）
├─app                  应用目录
│ ├─demo               应用目录
│ │ ├─controller       应用控制器目录
│ │ │ ├─Admin.php      Admin应用控制器
│ │ │ ├─Index.php      Index应用控制器
│ │ ├─view             应用视图目录
│ │ │ ├─admin          admin控制器目录
│ │ │ │ ├─index.html   index视图模板文件
│ │ │ ├─index          index控制器目录
│ │ │ │ ├─index.html   index视图模板文件
│ │ ├─common.php       函数定义文件
│ │ ├─middleware.php   中间件定义文件
│ │ ├─event.php        事件定义文件
```

### 快速生成控制器
```php
php think wym:controller demo@Demo
```

执行上面的指令会自动生成demo应用的Demo控制器类库及对应视图模板(默认index.html)文件。如下图：
```html
www  WEB部署目录（或者子目录）
├─app                 应用目录
│ ├─demo              应用目录
│ │ ├─controller      应用控制器目录
│ │ │ ├─Demo.php      Demo应用控制器
│ │ ├─view            应用视图目录
│ │ │ ├─demo          demo控制器目录
│ │ │ │ ├─index.html  index视图模板文件
```

### 快速生成模型器
```php
php think wym:model demo@Demo
```

执行上面的指令会自动生成demo应用的Demo模型器类库文件。如下图：
```html
www  WEB部署目录（或者子目录）
├─app             应用目录
│ ├─demo          应用目录
│ │ ├─model       模型目录
│ │ │ ├─Demo.php  Demo模型器类库文件
```

### 快速生成路由器
```php
php think wym:route demo
```

执行上面的指令会自动生成demo应用的Demo路由器类库文件。如下图：
```html
www  WEB部署目录（或者子目录）
├─app               应用目录
│ ├─demo            应用目录
│ │ ├─router        路由目录
│ │ │ ├─router.php  路由定义文件
```

### 快速生成验证器
```php
php think wym:validate demo@Demo
```

执行上面的指令会自动生成demo应用的Demo验证器类库文件。如下图：
```html
www  WEB部署目录（或者子目录）
├─app             应用目录
│ ├─demo          应用目录
│ │ ├─validate    验证器目录
│ │ │ ├─Demo.php  Demo验证器定义文件
```

### 快速生成视图模板
```php
php think wym:view demo@Demo[/index]
```

执行上面的指令会自动生成demo应用的视图模板(默认index.html)文件。如下图：
```html
www  WEB部署目录（或者子目录）
├─app                应用目录
│ ├─demo             应用目录
│ │ ├─view           应用视图目录
│ │ │ ├─demo         demo控制器目录
│ │ │ │ ├─index.html index视图模板文件
````