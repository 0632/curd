<?php

namespace wym\curd;

use think\console\command\Make;
use think\helper\Str;

class Curd extends Make
{
    protected $type;

    /**
     * 获取模板
     *
     * @return string
     */
    protected function getStub(): string
    {
        return __DIR__ . DIRECTORY_SEPARATOR . 'Stubs' . DIRECTORY_SEPARATOR . $this->type . '.stub';
    }

    /**
     * 获取类命名空间
     *
     * @param string $app
     *
     * @return string
     */
    protected function getNamespace(string $app): string
    {
        return parent::getNamespace($app) . '\\' . Str::lower($this->type);
    }

    /**
     * 创建目录
     *
     * @access protected
     *
     * @param string $dirname 目录名称
     */
    protected function checkDirBuild(string $dirname): void
    {
        if (!is_dir($dirname)) {
            mkdir($dirname, 0755, true);
        }
    }
}
