<?php

declare(strict_types=1);

namespace wym\curd\Command;

use think\console\Input;
use think\console\Output;
use wym\curd\Curd;
use think\helper\Str;

class View extends Curd
{
    protected $type = 'View';

    protected function configure(): void
    {
        parent::configure();
        $this->setName('wym:view')
             ->setDescription('Custom view');
    }

    protected function getPathName(string $name): string
    {
        $name = str_replace('app\\', '', $name);

        return $this->app->getBasePath() . ltrim(str_replace('\\', '/', $name), '/') . '.html';
    }

    protected function execute(Input $input, Output $output): void
    {
        $name = trim($input->getArgument('name'));

        $classname = $this->getClassName($name);

        $pathname = $this->getPathName($classname);

        if (is_file($pathname)) {
            $output->writeln('<error>' . $this->type . ':' . $classname . ' already exists!</error>');
        } else {
            if (!is_dir(dirname($pathname))) {
                mkdir(dirname($pathname), 0755, true);
            }
            file_put_contents($pathname, $this->buildClass($classname));

            $output->writeln('<info>' . $this->type . ':' . $classname . ' created successfully.</info>');
        }
    }

    protected function getClassName(string $name): string
    {
        if (str_contains($name, '\\')) {
            return $name;
        }

        if (strpos($name, '@')) {
            [$app, $name] = explode('@', $name);
        } else {
            $app = '';
        }

        if (strpos($name, '/')) {
            [$name, $html] = explode('/', $name);
        } else {
            $html = 'index';
        }

        if (str_contains($name, '/')) {
            $name = str_replace('/', '\\', $name);
        }

        return $this->getNamespace($app) . '\\' . Str::lower($name) . '\\' . Str::lower($html);
    }
}
