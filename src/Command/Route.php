<?php

declare(strict_types=1);

namespace wym\curd\Command;

use think\console\Input;
use think\console\input\Argument;
use think\console\Output;
use wym\curd\Curd;
use think\helper\Str;

class Route extends Curd
{
    protected $type = 'Route';

    protected function configure(): void
    {
        $this->setName('wym:route')
             ->addArgument('app', Argument::REQUIRED, 'app name .')
             ->setDescription('Custom App route');
    }

    protected function execute(Input $input, Output $output): void
    {
        $basePath = $this->app->getBasePath();
        $app      = $input->getArgument('app') ?: '';

        $appPath = $basePath . $app . DIRECTORY_SEPARATOR . 'route';
        $this->checkDirBuild($appPath);

        $filename = $appPath . DIRECTORY_SEPARATOR . Str::lower($this->type) . '.php';
        $info     = $this->type . ':' . str_replace('.php', '', str_replace(root_path(), '', $filename));

        if (!is_file($filename)) {
            $content = file_get_contents($this->getStub());

            file_put_contents($filename, $content);
            $info = '<info>' . $info . ' created successfully.</info>';
        } else {
            $info = '<error>' . $info . ' already exists!</error>';
        }
        $output->writeln($info);
    }
}
