<?php

declare (strict_types=1);

namespace wym\curd\Command;

use wym\curd\Curd;

class Model extends Curd
{

    protected $type = "Model";

    protected function configure(): void
    {
        parent::configure();
        $this->setName('wym:model')
             ->setDescription('Custom model');
    }

    protected function buildClass(string $name): string
    {
        $stub = file_get_contents($this->getStub());

        $namespace = trim(implode('\\', array_slice(explode('\\', $name), 0, -1)), '\\');

        $class = str_replace($namespace . '\\', '', $name);

        return str_replace(['{%className%}', '{%actionSuffix%}', '{%namespace%}', '{%app_namespace%}'], [
            $class,
            $this->app->config->get('route.action_suffix'),
            $namespace,
            trim(implode('\\', array_slice(explode('\\', $name), 0, -2)), '\\'),
        ], $stub);
    }
}
