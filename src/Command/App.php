<?php

declare(strict_types=1);

namespace wym\curd\Command;

use think\app\command\Build;
use think\console\Input;
use think\console\input\Argument;
use think\console\Output;
use think\facade\Console;

class App extends Build
{
    protected function configure(): void
    {
        // 指令配置
        $this->setName('app')
             ->addArgument('app', Argument::OPTIONAL, 'app name .')
             ->setDescription('Custom App Dirs');
    }

    protected function execute(Input $input, Output $output): void
    {
        $this->basePath = $this->app->getBasePath();
        $app            = $input->getArgument('app') ?: '';

        if (is_file($this->basePath . 'build.php')) {
            $list = include $this->basePath . 'build.php';
        } else {
            $list = [
                '__dir__' => ['controller', 'view'],
            ];
        }

        $this->buildApp($app, $list);
        Console::call('wym:controller', [$app . '@Admin']);
        Console::call('wym:controller', [$app . '@Index']);
        $output->writeln('<info>app created Successes</info>');
    }
}
