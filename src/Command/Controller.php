<?php

declare (strict_types=1);

namespace wym\curd\Command;

use think\console\Input;
use think\console\Output;
use think\facade\Console;
use wym\curd\Curd;

class Controller extends Curd
{

    protected $type = "Controller";

    protected function configure(): void
    {
        parent::configure();
        $this->setName('wym:controller')
             ->setDescription('Custom controller');
    }

    protected function execute(Input $input, Output $output): void
    {
        parent::execute($input, $output);
        Console::call('wym:view', [trim($input->getArgument('name'))]);
    }

    protected function getClassName(string $name): string
    {
        return parent::getClassName($name) . ($this->app->config->get('route.controller_suffix') ? 'Controller' : '');
    }
}
