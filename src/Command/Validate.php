<?php

declare (strict_types=1);

namespace wym\curd\Command;

use wym\curd\Curd;

class Validate extends Curd
{
    protected $type = 'Validate';

    protected function configure(): void
    {
        parent::configure();
        $this->setName('wym:validate')
             ->setDescription('Custom validate');
    }
}
