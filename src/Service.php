<?php

namespace wym\curd;

class Service extends \think\Service
{
    public function boot(): void
    {
        $commands = [
            'wym:app'        => Command\App::class,
            'wym:controller' => Command\Controller::class,
            'wym:model'      => Command\Model::class,
            'wym:route'      => Command\Route::class,
            'wym:validate'   => Command\Validate::class,
            'wym:view'       => Command\View::class,
        ];
        $this->commands($commands);
    }
}
